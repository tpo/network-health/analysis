# Onionperf dashboard

This projects creates a simple dashboard to compare differnt onionperf clients
metrics.

To install first install all the packages from the requirements.txt file:
```
$ pip install -r requirements.txt
```

Then download the data from the onionperfs with the `download-data` script:
```
$ ./bin/download-data
```

Then create the sqlite database with:
```
$ ./bin/create-db
```

Finally run the app server with:
```
$ python3 app.py
```

For sqlite3 on linux please do not use the debian/ubuntu package as this is outdated
and doesn't allow direct csv imports. Install instead from sources or binaries from:
https://www.sqlite.org/download.html
