# Run this app with `python app.py` and
# visit http://127.0.0.1:8050/ in your web browser.

from dash import Dash, html, dcc, Input, Output
from datetime import date
import plotly.express as px
import numpy as np
import pandas as pd
import onionperf.util as util
import os
import sqlite3
from sqlite3 import Error

app = Dash( __name__,
    meta_tags=[{"name": "viewport", "content": "width=device-width, initial-scale=1"}],
)
app.title = "Tor Network Performances"

def create_connection(db_file):
    """ create a database connection to the SQLite database
        specified by the db_file
    :param db_file: database file
    :return: Connection object or None
    """
    conn = None
    try:
        conn = sqlite3.connect(db_file)
    except Error as e:
        print(e)

    return conn

def select_all(table, conn, date_start, date_end, datasets):
    """
    Query all rows in the passed table
    :param table: the table to query from
    :param conn: the Connection object
    :param date_start: the start date
    :param date_end: the end_date
    :return: DataFrame
    """
    ds = []
    if isinstance(datasets, list):
        ds = datasets
    elif isinstance(datasets, str):
        ds = [datasets]

    query = "SELECT * FROM {} WHERE start>={} AND start>={} AND LABEL IN ({})".format(table, date_start, date_end, ", ".join([f'"{str(i)}"' for i in ds]))

    df = pd.read_sql_query(query, conn, coerce_float=True, parse_dates=['start'])
    df = df.replace(r'^\s*$', np.nan, regex=True)
    return df


labels = [
    "op-de7a",
    "op-hk7a",
    "op-de7a-2g-70cbt",
    "op-hk7a-2g-70cbt",
    "op-de7a-3g-66cbt",
    "op-hk7a-3g-66cbt",
]

app.layout = html.Div(children=[
    # Header
    html.Div(
        [
             html.Div(
                    [
                        html.H4("TOR NETWORK PERFORMANCES", className="app__header__title"),
                        html.P(
                            "We use OnionPerf to run performance measurements. This works by fetching files of different sizes over Tor and measuring how long that takes.",
                            className="app__header__title--grey",
                        ),
                    ],
                    className="app__header__desc",
                ),
             html.Div(
                    [
                        html.A(
                            html.Img(
                                src=app.get_asset_url("tor-onion-logo.png"),
                                className="app__menu__img",
                            ),
                            href="https://www.torproject.org",
                        ),
                    ],
                    className="app__header__logo",
                ),
        ],
        className="app__header",
    ),
    html.Div(
        [
            html.Div(
                # Dropdowns
                [
                    html.Div(
                        [
                            html.Div(
                                [html.P("server:")],
                            ),
                            html.Div(
                                [dcc.Dropdown(['onion', 'public'], 'public', id='server')],
                            ),
                        ],
                    ),
                    html.Div(
                        [
                            html.Div(
                                [html.P("instances:")],
                            ),
                            html.Div(
                                [dcc.Dropdown(labels, labels[0], id='datasets', multi=True)],
                            ),
                        ],
                    ),
                    html.Div(
                        [
                            html.Div(
                                [html.P("download file in bytes:")],
                            ),
                            html.Div(
                                [dcc.Dropdown(['5242880', '1048576'], '5242880', id='bytes')],
                            ),
                        ],
                    ),
                    html.Div(
                        [
                            html.Div(
                                [html.P("which metrics you want to graph?")],
                            ),
                            html.Div(
                                [dcc.Dropdown(['time_to_first_byte', 'time_to_last_byte', 'throughput_ecdf'], 'time_to_first_byte', id='graph_function')],
                            ),
                        ],
                    ),
                    html.Div(
                        [
                            html.Div(
                                [html.P("date range picker:")],
                            ),
                            html.Div(
                                [dcc.DatePickerRange(
                                    id='date-picker-range',
                                    min_date_allowed=date(2022, 7, 1),
                                    max_date_allowed=date(2022, 10, 29),
                                    initial_visible_month=date(2022, 10, 1),
                                    start_date=date(2022,10,1),
                                    end_date=date(2022, 10, 29)
                                ),],
                            ),
                        ]
                    ),
                ], className="one-third column",
            ),
            html.Div(
                [
                    # Graphs
                    html.Div(
                        [

                            dcc.Graph(
                                id="graph_function_container",
                            ),
                        ], className="graph__container"
                    ),
                ], className="two-thirds column"
            ),
        ], className="app__content",
    ),
    html.Div(
        [
            html.A(
                html.Button("SOURCE CODE", className="link-button"),
                href="https://gitlab.torproject.org/tpo/network-health/metrics/onionperf-dash",
            ),
            html.A(
                html.Button("Onionperf Data", className="link-button"),
                href="https://collector.torproject.org/archive/onionperf/",
            ),
        ],
    ),
], className="app__container",)

@app.callback(
    Output('graph_function_container', 'figure'),
    Input('server', 'value'),
    Input('datasets', 'value'),
    Input('bytes', 'value'),
    Input('graph_function', 'value'),
    Input('date-picker-range', 'start_date'),
    Input('date-picker-range', 'end_date'))
def update_graph_function_container(server, datasets, bytes, graph_function, start_date, end_date):
    if graph_function == "time_to_first_byte":
        fig = update_time_to_first_byte(server, datasets, start_date, end_date)
        fig.update_layout(transition_duration=500)
        return fig
    elif graph_function == "time_to_last_byte":
        fig = update_time_to_last_byte(server, datasets, bytes, start_date, end_date)
        fig.update_layout(transition_duration=500)
        return fig
    elif graph_function == "throughput_ecdf":
        fig = update_throughput_ecdf(server, datasets, start_date, end_date)
        fig.update_layout(transition_duration=500)
        return fig

def update_time_to_first_byte(server, datasets, start_date, end_date):
    connection = create_connection("database.db")
    df = select_all("measurements", connection, start_date, end_date, datasets)
    x="time_to_first_byte"
    hue="label"
    hue_name="Data set"
    title="Time to download first byte from {0} service".format(server)
    xlabel="Download time (s)"
    ylabel="Cumulative Fraction"
    ds = []
    if isinstance(datasets, list):
        ds = datasets
    elif isinstance(datasets, str):
        ds = [datasets]

    data = df.loc[df["server"] == server]
    data = data.loc[data['label'].isin(ds)]
    result = prepare_ecdf(data, x, hue, hue_name)
    fig = px.ecdf(result, x=x, y="rank_pct", color=hue_name)
    return fig

def update_time_to_last_byte(server, datasets, bytes, start_date, end_date):
    connection = create_connection("measurements.db")
    df = select_all("measurements", connection, start_date, end_date, datasets)
    x="time_to_last_byte"
    hue="label"
    hue_name="Data set"
    title="Time to download last of {1} byte from {0} service".format(server, bytes)
    xlabel="Download time (s)"
    ylabel="Cumulative Fraction"
    ds = []
    if isinstance(datasets, list):
        ds = datasets
    elif isinstance(datasets, str):
        ds = [datasets]

    data = df.loc[df["server"] == server]
    data = data.loc[data["filesize_bytes"] == int(bytes)]
    data = data.loc[data['label'].isin(ds)]
    result = prepare_ecdf(data, x, hue, hue_name)
    fig = px.ecdf(result, x=x, y="rank_pct", color=hue_name)
    return fig

def update_throughput_ecdf(server, datasets, start_date, end_date):
    connection = create_connection("measurements.db")
    df = select_all("measurements", connection, start_date, end_date, datasets)
    x="mbps"
    hue="label"
    hue_name="Data set"
    title="Throughput when downloading from {0} server".format(server)
    xlabel="Throughput (Mbps)"
    ylabel="Cumulative Fraction"
    ds = []
    if isinstance(datasets, list):
        ds = datasets
    elif isinstance(datasets, str):
        ds = [datasets]

    data = df.loc[df["server"] == server]
    data = data.loc[data['label'].isin(ds)]
    result = prepare_ecdf(data, x, hue, hue_name)
    fig = px.ecdf(result, x=x, y="rank_pct", color=hue_name)
    return fig

def prepare_ecdf(data, x, hue, hue_name):
    data = data.dropna(subset=[x])
    p0 = data[x].quantile(q=0.0, interpolation="lower")
    p99 = data[x].quantile(q=0.99, interpolation="higher")
    ranks = data.groupby(hue)[x].rank(pct=True)
    ranks.name = "rank_pct"
    result = pd.concat([data[[hue, x]], ranks], axis=1)
    result = result.append(pd.DataFrame({hue: data[hue].unique(),
                   x: p0 - (p99 - p0) * 0.05, "rank_pct": 0.0}),
                   ignore_index=True, sort=False)
    result = result.append(pd.DataFrame({hue: data[hue].unique(),
                   x: p99 + (p99 - p0) * 0.05, "rank_pct": 1.0}),
                   ignore_index=True, sort=False)
    result = result.rename(columns={hue: hue_name})

    return result

if __name__ == '__main__':
    app.run_server(debug=True)
