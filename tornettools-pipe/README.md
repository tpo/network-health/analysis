# Tornetools pipelines

The scripts in this folder are used to process various types of network data
and pipe results in order to be used by tornettools.

## Add ability to commit live data into sim-results

https://gitlab.torproject.org/jnewsome/sponsor-61-sims/-/issues/17

For the above issue we have used the script tnt-stage which is used to create
a tor_metrics.json file that will be used by tornettools to plot tor network
measured performances against shadow simulation results.

tnt-stage takes as input:
- the consensuses files for a given interval
- the server descriptors for the same interval
- onionperf analysis files for the same interval
- userstats for relays from metrics.tpo
- bandwidth measurements from metrics.tpo

The script requires:

autoconf automake bsdmainutils gcc libevent-dev libssl-dev make python3
python3-pip wget xz-utils zlib1g-dev

The script is run with:

```
$ ./tnt-stage -d <day> -m <year-month> -o <folder> -p <onionperf_data_path> -f <filters> -r <roles>
```

Ex:
```
./tnt-stage -f ~/Workspace/congestion-control/data/2022-06-16-g20-f50-fast_excl.txt -r fast -d 12 -m 2022-06 -o data
```

tnt-stage will download the needed information from metrics.tpo and collector.torproject.org

If a path to onionperf.analysis.json.xz files is provided these files won't be
downloaded. You can also specify the onionperf instance(s) you want to download
measurements from using the switch ``-i <instance-1> <instance-2>``.
If you specify one or more instances these will all be included in the filtering
and will generate a combined tor_metrics_.json file


If a list of fingerprints to exclude is provided you can also provide rose that
these node need to play in the circuit in order to be excluded.

Accepted roles are:
- fast
- guard
- exit

These are applied to the command:
```
$ onionperf filter <options>
```

Please see: https://gitlab.torproject.org/tpo/network-health/metrics/onionperf#filtering-measurement-results
