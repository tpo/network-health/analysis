# Network health analysis

This repository contains different scripts that have been used for various
network health analysis.

## Introduction

Each folder in this repository contains a number of scripts that have been used
for one or more issues in the analysis repository.

Each folder also contains a README.md file that will explain which issue(s) were
solved with the code in the folder and how to run it.

## TOC

- [OnionPerf dashboard](./perfs)
- [Tornettools pipeline](./tornettools-pipe)
